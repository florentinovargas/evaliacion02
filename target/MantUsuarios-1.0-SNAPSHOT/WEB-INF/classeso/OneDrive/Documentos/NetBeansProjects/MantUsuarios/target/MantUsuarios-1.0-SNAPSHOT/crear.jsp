<%-- 
    Document   : index
    Created on : 20-abr-2020, 20:04:00
    Author     : junior
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Test Lista</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Crear</h1>
        <p class="">
            
   <form  name="form" action="controllerUsuario" method="POST">
                    <div class="form-group">
                        <label for="rut">Rut</label>
                        <input  name="rut" class="form-control" required id="rut" aria-describedby="rutHelp">
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  step="any" name="nombre" class="form-control" required id="nombre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="apellido1">Apellido Paterno</label>
                         <input  name="apellido1" class="form-control" required id="apellido1" aria-describedby="apellido1Help">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="apellido2">Apellido Materno</label>
                         <input  name="apellido2" class="form-control" required id="apellido2" aria-describedby="apellido2Help">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="correo">Correo</label>
                         <input  name="correo" class="form-control" required id="correo" aria-describedby="correoHelp">
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="fono">Telefono</label>
                         <input  name="fono" class="form-control" required id="fono" aria-describedby="fonoHelp">
                 
                      </div>        
                    <br>
                    <button type="submit" type="submit" name="accion" value="grabarCrear" class="btn btn-success">Grabar</button>
                           <button type="submit" class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>